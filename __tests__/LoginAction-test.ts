import {cloneableGenerator} from '@redux-saga/testing-utils';
import {put, call} from 'redux-saga/effects';
import api from '../src/services/api';
import * as actions from '../src/store/login/actions';
import {loginSaga} from '../src/store/login/sagas';
import * as types from '../src/store/login/types';

const user = {
  email: 'email@teste.com',
  password: '123456',
};

describe('create action', () => {
  it('should create an action to make login', () => {
    const expectedAction = {
      type: types.LOGIN_USER,
      payload: user,
    };

    expect(actions.loginAction(user)).toEqual(expectedAction);
  });
});

describe('login flow', () => {
  const generator = cloneableGenerator(loginSaga)(user);
  expect(generator.next().value).toEqual(call(api.post, 'login', user));

  test('credentials success', () => {
    const clone = generator.clone();
    expect(clone.next(true).value).toEqual(
      put({type: types.LOGIN_USER_SUCCESS, payload: {success: true}}),
    );
    expect(clone.next().done).toEqual(true);
  });

  test('credentials error', () => {
    const error = {message: 'error'};
    const clone = generator.clone();
    expect(clone.throw(error).value).toEqual(
      put({type: types.LOGIN_USER_ERROR, payload: {error: error.message}}),
    );
    expect(clone.next().done).toEqual(true);
  });
});
