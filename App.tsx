import React from 'react';
import Login from './src/screens/Login';
import {Provider} from 'react-redux';
import configureStore from './src/store';

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <Login />
    </Provider>
  );
};

export default App;
