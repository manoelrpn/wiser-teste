import {Alert} from 'react-native';
import axios from 'axios';

export const BASE_URL = 'https://60331da5a223790017acfbfb.mockapi.io';

const instance = axios.create({
  baseURL: `${BASE_URL}/api/v1/`,
  timeout: 2 * 60 * 1000,
  validateStatus: function (status) {
    return status < 400;
  },
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    Alert.alert('Ops', 'Algo deu errado');

    return Promise.reject(error);
  },
);

export default instance;
