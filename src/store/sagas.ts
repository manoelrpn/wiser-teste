import {takeLatest, fork} from 'redux-saga/effects';
import {loginSaga} from './login/sagas';
import * as types from './login/types';

function* watchLogin() {
  yield takeLatest(types.LOGIN_USER, loginSaga);
}

export default function* startWatch() {
  yield fork(watchLogin);
}
