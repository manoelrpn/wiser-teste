export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR';

export interface LoginState {
  email: string;
  password: string;
  loading: boolean;
  success: boolean;
  error: string;
}

export interface IUser {
  email: string;
  password: string;
}

export interface ILoginError {
  error: string;
}

export interface ILoginSuccess {
  success: boolean;
}

interface LoginAction {
  type: typeof LOGIN_USER;
  payload: IUser;
}

interface LoginSuccessAction {
  type: typeof LOGIN_USER_SUCCESS;
  payload: ILoginSuccess;
}

interface LoginErrorAction {
  type: typeof LOGIN_USER_ERROR;
  payload: ILoginError;
}

export type LoginActionTypes =
  | LoginAction
  | LoginSuccessAction
  | LoginErrorAction;
