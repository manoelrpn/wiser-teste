import {put, call} from 'redux-saga/effects';
import {Alert} from 'react-native';
import api from '../../services/api';

import * as types from './types';

export function* loginSaga(payload: types.IUser) {
  try {
    const response = yield call(api.post, 'login', payload);

    if (response.status === 201) {
      Alert.alert('Sucesso', 'Login realizado com sucesso');
      // we can now store user information
    }

    yield put({type: types.LOGIN_USER_SUCCESS, payload: {success: true}});
  } catch (error) {
    yield put({type: types.LOGIN_USER_ERROR, payload: {error: error.message}});
  }
}
