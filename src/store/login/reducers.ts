import * as types from './types';

const initialState: types.LoginState = {
  email: '',
  password: '',
  loading: false,
  success: false,
  error: '',
};

export default function (
  state = initialState,
  action: types.LoginActionTypes,
): types.LoginState {
  switch (action.type) {
    case types.LOGIN_USER:
      return {
        ...state,
        ...action.payload,
        loading: true,
      };
    case types.LOGIN_USER_SUCCESS:
      return {
        ...state,
        ...action.payload,
        loading: false,
      };
    case types.LOGIN_USER_ERROR:
      return {
        ...state,
        ...action.payload,
        loading: false,
      };
    default:
      return state;
  }
}
