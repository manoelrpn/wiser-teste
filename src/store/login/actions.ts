import * as types from './types';

export function loginAction(user: types.IUser): types.LoginActionTypes {
  return {
    type: types.LOGIN_USER,
    payload: user,
  };
}

export function loginSuccessAction(
  success: types.ILoginSuccess,
): types.LoginActionTypes {
  return {
    type: types.LOGIN_USER_SUCCESS,
    payload: success,
  };
}

export function loginErrorAction(
  error: types.ILoginError,
): types.LoginActionTypes {
  return {
    type: types.LOGIN_USER_ERROR,
    payload: error,
  };
}
