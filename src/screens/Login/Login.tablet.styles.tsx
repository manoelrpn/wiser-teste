import React from 'react';
import {ActivityIndicator} from 'react-native';
import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import LoginBackground from '../../assets/images/login_background_tablet.png';
import ImageBackground from '../../assets/images/background.png';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import {LoginProps} from './types';

export default function Login({
  handleSubmit,
  setValue,
  errors,
  trigger,
  isValid,
  onSubmit,
  loading,
}: LoginProps): JSX.Element {
  return (
    <>
      <Container>
        <Image resizeMode="cover" source={LoginBackground} />
        <Content>
          <Title>Olá, seja {'\n'}bem-vindo!</Title>
          <SubTitle>Para acessar a plataforma,{'\n'}faça seu login.</SubTitle>
          <TextInputWrapper marginTop={40}>
            <TextInput
              label="E-MAIL"
              placeholder="Digite um e-mail"
              onChangeText={(text: string) => {
                setValue('email', text);
                trigger('email');
              }}
              error={errors?.email?.message}
              autoCapitalize="none"
            />
          </TextInputWrapper>
          <TextInputWrapper marginTop={20}>
            <TextInput
              label="SENHA"
              placeholder="Digite uma senha"
              secureTextEntry
              onChangeText={(text: string) => {
                setValue('password', text);
                trigger('password');
              }}
              error={errors?.password?.message}
            />
          </TextInputWrapper>
          <ButtonWrapper
            disabled={!isValid || loading}
            activeOpacity={0.5}
            onPress={handleSubmit(onSubmit)}>
            <Button
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#9D25B0', '#383E71']}>
              <ButtonText>ENTRAR</ButtonText>
              {loading && <ButtonLoading color="white" size="small" />}
            </Button>
          </ButtonWrapper>
          <ForgotPasswordContainer onPress={() => {}}>
            <ForgotPasswordText>
              Esqueceu seu login ou senha?{'\n'}Clique{' '}
              <UnderlineText>aqui</UnderlineText>
            </ForgotPasswordText>
          </ForgotPasswordContainer>
        </Content>
      </Container>
      <BackgroundImage source={ImageBackground} />
    </>
  );
}

const Container = styled.View`
  flex: 1;
  flex-direction: row;
`;

const Image = styled.Image`
  height: 100%;
  width: 327px;
`;

const BackgroundImage = styled.Image`
  position: absolute;
  left: 0;
  height: 100%;
  z-index: 999;
  width: 327px;
`;

const Content = styled.View`
  flex: 1;
  background-color: #faf5ff;
  border-radius: 8px;
  justify-content: center;
  padding-horizontal: 80px;
`;

const Title = styled(Text)`
  font-weight: 400;
  font-size: 40px;
  line-height: 48px;
  color: #383e71;
`;

const SubTitle = styled(Text)`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  margin-top: 16px;
  color: #989fdb;
`;

const TextInputWrapper = styled.View`
  margin-top: ${({marginTop}: {marginTop: number}) => marginTop || 0}px;
  margin-bottom: ${({marginBottom}: {marginBottom: number}) =>
    marginBottom || 0}px;
`;

const ButtonWrapper = styled.TouchableOpacity`
  margin-top: 24px;
  opacity: ${({disabled}: {disabled: boolean}) => (disabled ? 0.8 : 1)};
`;

const Button = styled(LinearGradient)`
  border-radius: 8px;
  width: 100%;
  height: 48px;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

const ButtonLoading = styled.ActivityIndicator`
  margin-left: 8px;
`;

const ButtonText = styled(Text)`
  font-size: 16px;
  font-weight: 600;
  color: #fff;
  line-height: 20px;
`;

const ForgotPasswordContainer = styled.TouchableOpacity`
  margin-top: 32px;
`;

const ForgotPasswordText = styled(Text)`
  font-weight: 400;
  text-align: center;
  font-size: 14px;
  line-height: 20px;
  color: #989fdb;
`;

const UnderlineText = styled(ForgotPasswordText)`
  text-decoration: underline;
  text-decoration-color: #9d25b0;
  color: #9d25b0;
`;
