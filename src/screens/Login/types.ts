import {DeepMap, FieldError} from 'react-hook-form';

export interface IFormInputs {
  email: string;
  password: string;
}

export interface LoginProps {
  handleSubmit: Function;
  setValue: Function;
  errors: DeepMap<IFormInputs, FieldError>;
  trigger: Function;
  isValid: boolean;
  onSubmit: Function;
  loading: boolean;
}
