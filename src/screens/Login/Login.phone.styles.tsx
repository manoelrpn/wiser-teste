import React from 'react';
import {Dimensions} from 'react-native';
import styled from 'styled-components/native';
import {Card} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import LoginBackground from '../../assets/images/login_background.png';
import ImageBackground from '../../assets/images/background.png';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import {LoginProps} from './types';

const {height, width} = Dimensions.get('window');

export default function Login({
  handleSubmit,
  setValue,
  errors,
  trigger,
  isValid,
  onSubmit,
  loading,
}: LoginProps): JSX.Element {
  return (
    <>
      <BackgroundImage
        zIndex={-500}
        resizeMode="cover"
        source={LoginBackground}
      />
      <Container>
        <CustomCard>
          <Title>Olá, seja {'\n'}bem-vindo!</Title>
          <SubTitle>Para acessar a plataforma, faça seu login.</SubTitle>
          <TextInputWrapper marginTop={20}>
            <TextInput
              label="E-MAIL"
              placeholder="Digite um e-mail"
              onChangeText={(text: string) => {
                setValue('email', text);
                trigger('email');
              }}
              error={errors?.email?.message}
              autoCapitalize="none"
            />
          </TextInputWrapper>
          <TextInputWrapper marginTop={30} marginBottom={57}>
            <TextInput
              label="SENHA"
              placeholder="Digite uma senha"
              secureTextEntry
              onChangeText={(text: string) => {
                setValue('password', text);
                trigger('password');
              }}
              error={errors?.password?.message}
            />
          </TextInputWrapper>
          <ButtonWrapper
            disabled={!isValid || loading}
            activeOpacity={0.5}
            onPress={handleSubmit(onSubmit)}>
            <Button
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#9D25B0', '#383E71']}>
              <ButtonText>ENTRAR</ButtonText>
              {loading && <ButtonLoading color="white" size="small" />}
            </Button>
          </ButtonWrapper>
        </CustomCard>
        <ForgotPasswordContainer onPress={() => {}}>
          <ForgotPasswordText>
            Esqueceu seu login ou senha?{'\n'}Clique{' '}
            <UnderlineText>aqui</UnderlineText>
          </ForgotPasswordText>
        </ForgotPasswordContainer>
      </Container>
      <BackgroundImage zIndex={-100} source={ImageBackground} />
      <Footer />
    </>
  );
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  padding-horizontal: 32px;
`;

const CustomCard = styled(Card)`
  background-color: #faf5ff;
  border-radius: 8px;
`;

const BackgroundImage = styled.Image`
  position: absolute;
  top: 0;
  height: ${height / 2}px;
  width: 100%;
  z-index: ${({zIndex}: {zIndex: number}) => zIndex ?? 1};
`;

const Footer = styled.View`
  position: absolute;
  bottom: 0;
  height: ${height / 2}px;
  width: 100%;
  background-color: #130525;
  z-index: -999;
`;

const Title = styled(Text)`
  font-weight: 400;
  font-size: 24px;
  line-height: 32px;
  text-align: center;
  margin-top: 24px;
  color: #383e71;
`;

const SubTitle = styled(Text)`
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
  text-align: center;
  margin-top: 16px;
  color: #989fdb;
`;

const TextInputWrapper = styled.View`
  padding-horizontal: 28px;
  margin-top: ${({marginTop}: {marginTop: number}) => marginTop || 0}px;
  margin-bottom: ${({marginBottom}: {marginBottom: number}) =>
    marginBottom || 0}px;
`;

const ButtonWrapper = styled.TouchableOpacity`
  position: absolute;
  left: ${width / 2 - 160}px;
  bottom: -24px;
`;

const Button = styled(LinearGradient)`
  flex-direction: row;
  border-radius: 8px;
  width: 256px;
  height: 48px;
  align-items: center;
  justify-content: center;
`;

const ButtonLoading = styled.ActivityIndicator`
  margin-left: 8px;
`;

const ButtonText = styled(Text)`
  font-size: 16px;
  font-weight: 600;
  color: #fff;
  line-height: 20px;
`;

const ForgotPasswordContainer = styled.TouchableOpacity`
  margin-top: 48px;
`;

const ForgotPasswordText = styled(Text)`
  font-weight: 400;
  text-align: center;
  font-size: 14px;
  line-height: 20px;
  color: #fff;
`;

const UnderlineText = styled(ForgotPasswordText)`
  text-decoration: underline;
  text-decoration-color: #fff;
`;
