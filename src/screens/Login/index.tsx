import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from 'yup';
import {isTablet} from 'react-native-device-info';
import LoginPhone from './Login.phone.styles';
import LoginTablet from './Login.tablet.styles';
import {IFormInputs} from './types';
import {loginAction} from '../../store/login/actions';
import {LoginState} from '../../store/login/types';

const schema = yup.object().shape({
  email: yup
    .string()
    .email('Digite um e-mail válido')
    .required('O e-mail é obrigatório'),
  password: yup
    .string()
    .min(6, 'A senha deve conter no mínimero 6 caracteres')
    .required('A senha é obrigatória'),
});

export default function Login(): JSX.Element {
  const dispatch = useDispatch();
  const {loading} = useSelector(({login}: {login: LoginState}) => login);
  const {
    handleSubmit,
    register,
    setValue,
    errors,
    trigger,
    formState,
  } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
    mode: 'onSubmit',
  });

  const {isValid} = formState;

  React.useEffect(() => {
    register({name: 'email'});
    register({name: 'password'});
  }, [register]);

  const onSubmit = ({email, password}: IFormInputs) => {
    dispatch(loginAction({email, password}));
  };

  if (isTablet()) {
    return (
      <LoginTablet
        handleSubmit={handleSubmit}
        setValue={setValue}
        errors={errors}
        trigger={trigger}
        isValid={isValid}
        onSubmit={onSubmit}
        loading={loading}
      />
    );
  }

  return (
    <LoginPhone
      handleSubmit={handleSubmit}
      setValue={setValue}
      errors={errors}
      trigger={trigger}
      isValid={isValid}
      onSubmit={onSubmit}
      loading={loading}
    />
  );
}
