import React from 'react';
import {TextInputProps} from 'react-native';
import styled from 'styled-components/native';
import Text from '../Text';

interface TextInputInterface extends TextInputProps {
  label: string;
  error?: string;
}

export default function TextInput({
  label,
  error,
  ...props
}: TextInputInterface) {
  return (
    <>
      <Label>{label}</Label>
      <InputContainer borderColor={error ? '#ff377f' : null}>
        <Input {...props} />
      </InputContainer>
      {!!error && <ErrorMessage>{error}</ErrorMessage>}
    </>
  );
}

const InputContainer = styled.View`
  border-radius: 8px;
  border-width: 1px;
  border-color: ${({borderColor}: {borderColor: string}) =>
    borderColor ?? '#989fdb'};
  margin-top: 8px;
`;

const Input = styled.TextInput`
  height: 48px;
  padding-horizontal: 16px;
`;

const Label = styled(Text)`
  font-size: 10px;
  font-weight: 400;
`;

const ErrorMessage = styled(Text)`
  margin-top: 8px;
  color: #ff377f;
  margin-left: 10px;
  font-size: 10px;
`;
