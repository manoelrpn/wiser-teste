import React from 'react';
import styled from 'styled-components/native';

const Text = styled.Text``;

const DefaultText = styled(({...rest}) => <Text {...rest} />)`
  font-family: Montserrat-Regular;
`;

export default DefaultText;
